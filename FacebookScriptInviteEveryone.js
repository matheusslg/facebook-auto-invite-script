// Made by Matheus Cavallini and Leonardo Machado

// Facebook references that can change:
var personLineRef = '_5i_q'; // person line class
var seeMoreButtonRef = 'uiMorePager'; // "See More" button ID
var intervalToCheckSeeMoreButton = 3000; // time interval to click the "See More" button in milliseconds

///////////////////////////////
// DO NOT CHANGE BELOW!!!
///////////////////////////////

console.log('[FACEBOOK AUTO INVITE SCRIPT] Executing invite script Facebook...');
console.log('[FACEBOOK AUTO INVITE SCRIPT] Warning: to prevent Facebook from blocking the invitation action, there is a time interval between invitations');
console.log("[FACEBOOK AUTO INVITE SCRIPT] Choose one option:")
console.log("- If you want to run this code only once, write init() and press Enter");
console.log("- If you want to run this code forever, write init(true) and press Enter >> OBS: To stop this function, use stop() and press Enter");

var totalInvites = 0;
var seeMoreCheck;
var init = function(recursive) {
    seeMoreCheck = setInterval(function () {
        var seeMore = document.getElementsByClassName(seeMoreButtonRef)[0];
        var hasSeeMore = false;
        if (seeMore) {
            if (seeMore.innerHTML != '') {
                hasSeeMore = true;
            }
        }
        if (hasSeeMore) {
            executeInvites();
            seeMore.children[0].children[0].click();
            console.log("'See More' button clicked...");
        } else {
            executeInvites();
            clearInterval(seeMoreCheck);
            console.log('Total Invited:', totalInvites);
            if (recursive) {
                console.log('Waiting 5 seconds to execute again...');
                sleep(5000);
                init(true);
            } else {
                console.log('FINISH!');
            }
        }
    }, intervalToCheckSeeMoreButton);
}

var stop = function() {
    clearInterval(seeMoreCheck);
}

var sleep = function (milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i > -1; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

var executeInvites = function () {
    var peopleLikes = Array.from(document.getElementsByClassName(personLineRef));
    var invites = 0;

    peopleLikes.forEach(function (_personLike) {
        if (_personLike.children[0].children[1].children[0].children[1].children[1].children[0].children[0].tagName === "A") {
            invites++;
            var sleepTimer = Math.floor(Math.random() * 3000) + 2000;
            console.log('Inviting ' + _personLike.children[0].children[1].children[0].children[0].innerText + ' in: ' + (sleepTimer / 1000) + ' seconds...');
            sleep(sleepTimer);
            _personLike.children[0].children[1].children[0].children[1].children[1].children[0].children[0].click();
        }
    });
    console.log('Invited:', invites);
    totalInvites += invites;
    console.log('Total invitations so far:', totalInvites);
}